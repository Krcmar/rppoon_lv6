﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZAD3
{
    class CareTaker
    {
        private List<Memento> states = new List<Memento>();

        public void StoreState(Memento state)
        {
            states.Add(state);
        }

        public Memento GetState()
        {
            if (states.Count == 0)
            {
                Console.WriteLine("-----");
                return null;
            }
            else
            {
                return states.Last();
            }
        }
    }
}
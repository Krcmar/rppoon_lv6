﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZAD3
{
    class Program
    {
        static void Main(string[] args)
        {
            //treci
            //CareTaker careTaker = new CareTaker();
            //ToDoItem item = new ToDoItem("DVD", "First volume of One Piece", DateTime.Now);
            //careTaker.GetState();

            //careTaker.StoreState(item.StoreState());
            //Console.WriteLine(item);
            //item.Rename("BluRay disk");
            //Console.WriteLine(item);

            //item.RestoreState(careTaker.GetState());
            //Console.WriteLine(item);

            //cetvrti
            BankAccountCareTaker careTaker = new BankAccountCareTaker();
            BankAccount bankAccount = new BankAccount("Luffy", "New World", decimal.MinusOne);
            careTaker.GetState();

            careTaker.StoreState(bankAccount.StoreState());
            Console.WriteLine(bankAccount);
            bankAccount.ChangeOwnerAddress("Nami");
            bankAccount.UpdateBalance(100);
            Console.WriteLine(bankAccount);

            bankAccount.RestoreState(careTaker.GetState());
            Console.WriteLine(bankAccount);
        }
    }
}

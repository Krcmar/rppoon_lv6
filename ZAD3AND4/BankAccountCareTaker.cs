﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZAD3
{
    class BankAccountCareTaker
    {
        private List<BankAccountMemento> states = new List<BankAccountMemento>();

        public void StoreState(BankAccountMemento state)
        {
            states.Add(state);
        }

        public BankAccountMemento GetState()
        {
            if (states.Count == 0)
            {
                Console.WriteLine("-----");
                return null;
            }
            else
            {
                return states.Last();
            }
        }
    }
}

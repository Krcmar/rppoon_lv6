﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV6
{
    class BoxProgram
    {
        static void Main(string[] args)
        {
            //1 zad:
            //Notebook notebook = new Notebook();
            //notebook.AddNote(new Note("Note to self:", "study."));
            //notebook.AddNote(new Note("On the other hand:", "academia should burn."));
            //notebook.AddNote(new Note("You'll pull through.", "Dead or alive doesn't matter."));
            //notebook.AddNote(new Note("Dead more likely.", "Rest in peace."));

            //Iterator iterator = new Iterator(notebook);

            //for (Note note = iterator.First(); iterator.IsDone == false; note = iterator.Next())
            //{
            //    note.Show();
            //}

            Box box = new Box();
            box.AddProduct(new Product("Igrica", 1.11));
            box.AddProduct(new Product("DVD", 2.22));
            box.AddProduct(new Product("Anime figurica", 3.33));
            box.AddProduct(new Product("Manga", 4.44));

            ProductIterator iterator = new ProductIterator(box);

            for (Product product = iterator.First(); iterator.IsDone == false; product = iterator.Next())
            {
                Console.WriteLine(product.ToString());
            }

        }
    }
}

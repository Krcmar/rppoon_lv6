﻿using System;

namespace ZAD5
{
    class Program
    {
        static void Main(string[] args)
        {
            AbstractLogger logger = new ConsoleLogger(MessageType.ALL);
            FileLogger fileLogger = new FileLogger(MessageType.ERROR | MessageType.WARNING, "logFile.txt");

            logger.SetNextLogger(fileLogger);
            logger.Log("This is a message", MessageType.ALL);
            logger.Log("Errors are caused by mistakes!", MessageType.ERROR | MessageType.WARNING);
            logger.Log("Water boils at 100 degrees Celsius", MessageType.INFO);
            logger.Log("People wear masks and it's hard to say which one is real.", MessageType.WARNING);
            logger.Log("Too much thinking, abort mission!", MessageType.ERROR);
        }
    }
}

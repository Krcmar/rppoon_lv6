﻿using System;

namespace ZAD6
{
    class Program
    {
        static void Main(string[] args)
        {
            StringChecker lowerCaseChecker = new StringLowerCaseChecker();
            StringChecker upperCaseChecker = new StringUpperCaseChecker();
            StringChecker lengthChecker = new StringLengthChecker(5);
            StringChecker digitChecker = new StringDigitChecker();

            lowerCaseChecker.SetNext(upperCaseChecker);
            upperCaseChecker.SetNext(lengthChecker);
            lengthChecker.SetNext(digitChecker);

            Console.WriteLine(lowerCaseChecker.Check("Tea123"));
            Console.WriteLine(lowerCaseChecker.Check("tea123"));

            PasswordValidator validator = new PasswordValidator(lowerCaseChecker);

            validator.AddChecker(lowerCaseChecker, upperCaseChecker);
            validator.AddChecker(upperCaseChecker, lengthChecker);
            validator.AddChecker(lengthChecker, digitChecker);

            Console.WriteLine(validator.Check("Tea123"));
        }
    }
}

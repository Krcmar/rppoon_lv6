﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZAD6
{
    class StringLengthChecker : StringChecker
    {
        public int MinLength { get; private set; }

        public StringLengthChecker(int MinLength)
        {
            this.MinLength = MinLength;
        }

        protected override bool PerformCheck(string stringToCheck)
        {
            return stringToCheck.Length >= this.MinLength;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZAD6
{
    class StringDigitChecker : StringChecker
    {
        protected override bool PerformCheck(string stringToCheck)
        {
            bool hasDigit = false;
            foreach (char letter in stringToCheck)
            {
                if (char.IsDigit(letter)) hasDigit = true;
            }
            return hasDigit;
        }
    }
}

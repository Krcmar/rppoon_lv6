﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZAD6
{
    class StringLowerCaseChecker : StringChecker
    {
        protected override bool PerformCheck(string stringToCheck)
        {
            bool hasLower = false;
            foreach (char letter in stringToCheck)
            {
                if (char.IsLower(letter)) hasLower = true;
            }
            return hasLower;
        }
    }
}

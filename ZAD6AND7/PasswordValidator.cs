﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZAD6
{
    class PasswordValidator
    {
        private StringChecker checker;

        public PasswordValidator(StringChecker checker)
        {
            this.checker = checker;
        }

        public void AddChecker(StringChecker checker, StringChecker next)
        {
            checker.SetNext(next);
        }

        public bool Check(string stringToCheck)
        {
            return this.checker.Check(stringToCheck);
        }
    }
}
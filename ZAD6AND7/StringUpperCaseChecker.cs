﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZAD6
{
    class StringUpperCaseChecker : StringChecker
    {
        protected override bool PerformCheck(string stringToCheck)
        {
            bool hasUpper = false;
            foreach (char letter in stringToCheck)
            {
                if (char.IsUpper(letter)) hasUpper = true;
            }
            return hasUpper;
        }
    }
}